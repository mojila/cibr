import cv2
import numpy as np
import scipy
from imageio import imread
import pickle
import random
import os
import matplotlib.pyplot as plt
from scipy import spatial

# Feature extractor
def extract_features(image_path=None, image=None, vector_size=64):
    if image is None:
        image = imread(image_path, pilmode="RGB")
    try:
        # Using KAZE, cause SIFT, ORB and other was moved to additional module
        # which is adding addtional pain during install
        detector = cv2.KAZE_create()
        descriptor = cv2.xfeatures2d.FREAK_create()
        # Dinding image keypoints
        kps = detector.detect(image)
        # Getting first 32 of them.
        # Number of keypoints is varies depend on image size and color pallet
        # Sorting them based on keypoint response value(bigger is better)
        kps = sorted(kps, key=lambda x: -x.response)[:vector_size]
        # computing descriptors vector
        kps, dsc = descriptor.compute(image, kps)
        # Flatten all of them in one big vector - our feature vector
        dsc = dsc.flatten()
        # Making descriptor of same size
        # Descriptor vector size is 64
        needed_size = (vector_size * 64)
        if dsc.size < needed_size:
            # if we have less the 32 descriptors then just adding zeros at the
            # end of our feature vector
            dsc = np.concatenate([dsc, np.zeros(needed_size - dsc.size)])
    except cv2.error as e:
        print('Error: ', e)
        return None

    return dsc


def batch_extractor(images_path, pickled_db_path="./features.pck"):
    files = [os.path.join(images_path, p) for p in sorted(os.listdir(images_path))]

    result = {}
    for f in files:
        print(f'Extracting features from image {f}')
        name = f.split('/')[-1].lower()
        result[name] = extract_features(f)

    # saving all our feature vectors in pickled file
    with open(pickled_db_path, 'wb') as fp:
        pickle.dump(result, fp)


class Matcher(object):

    def __init__(self, pickled_db_path="./features.pck"):
        with open(pickled_db_path, 'rb') as fp:
            self.data = pickle.load(fp)
        self.names = []
        self.matrix = []
        for k, v in self.data.items():
            self.names.append(k)
            self.matrix.append(v)
        self.matrix = np.array(self.matrix)
        self.names = np.array(self.names)

    def cos_cdist(self, vector):
        # getting cosine distance between search image and images database
        v = vector.reshape(1, -1)
        return spatial.distance.cdist(self.matrix, v, 'cosine').reshape(-1)

    def match(self, image, topn=5):
        features = extract_features(image=image)
        img_distances = self.cos_cdist(features)
        # getting top 5 records
        nearest_ids = np.argsort(img_distances)[:topn].tolist()
        nearest_img_paths = self.names[nearest_ids].tolist()

        return nearest_img_paths, img_distances[nearest_ids].tolist()

def show_img(path):
    img = imread(path, mode="RGB")
    plt.imshow(img)
    plt.show()

def run():
    images_path = './dataset/'
    files = [os.path.join(images_path, p) for p in sorted(os.listdir(images_path))]
    # getting 3 random images 
    sample = random.sample(files, 3)

    batch_extractor(images_path)

    # ma = Matcher('./features.pck')

    # for s in sample:
    #    print('Query image ==========================================')
    #    show_img(s)
    #    names, match = ma.match(s, topn=15)
    #    print('Result images ========================================')
    #    for i in range(15):
    #        # we got cosine distance, less cosine distance between vectors
    #        # more they similar, thus we subtruct it from 1 to get match value
    #        print(f'Match {(1-match[i])}')
    #        show_img(os.path.join(images_path, names[i]))


import falcon
from io import BytesIO
import json
import cbir

class ImageRetrievalResource:

    def on_post(self, req, resp):
        try:
            matcher = cbir.Matcher('./features.pck')
            im = imread(BytesIO(req.stream.read()))
            names, match = matcher.match(im, topn=15)
            resp.data = json.dumps({'result': names})
        except Exception as e:
            print(e)

api = application = falcon.API()
api.add_route('/image-retrieval/', ImageRetrievalResource)

import hug
from PIL import Image
from hug.middleware import CORSMiddleware

@hug.response_middleware()
def process_data(request, response, resource):
    response.set_header('Access-Control-Allow-Origin', '*')

@hug.post('/image-retrieval/')
def digest(body):
    matcher = cbir.Matcher('./features.pck')
    im = imread(BytesIO(body['image']))
    names, match = matcher.match(im, topn=5)
    names = [name for name in names]
    return {'result': names}
