import cv2
import numpy as np
import scipy
from imageio import imread
import pickle
import random
import os
import matplotlib.pyplot as plt
from scipy import spatial
import mahotas
from PIL import Image

# Feature extractor

def hu_moments(im):
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    feature = cv2.HuMoments(cv2.moments(im)).flatten()
    return feature

def haralick(im):
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    feature = mahotas.features.haralick(im).mean(axis=0)
    return feature

def histogram(im):
    im = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
    feature = cv2.calcHist([im], [0, 1, 2], None, [10, 10, 10], [0, 256, 0, 256, 0, 256])
    cv2.normalize(feature, feature)
    return feature.flatten()

def extract_features(image_path=None, image=None, vector_size=32):
    if image is None:
        image = imread(image_path, pilmode='RGB')
    f1 = hu_moments(image)
    f2 = haralick(image)
    f3 = histogram(image)
    # f4 = loc(image=image)
    # print(f1.shape, f2.shape, f3.shape, f4.shape)
    features = np.hstack((f1, f2, f3))
    print(features.shape)
    return features

def loc(image_path=None, image=None, vector_size=32):
    if image is None:
        image = imread(image_path, pilmode="RGB")
    try:
        # Using KAZE, cause SIFT, ORB and other was moved to additional module
        # which is adding addtional pain during install
        detector = cv2.KAZE_create()
        descriptor = cv2.xfeatures2d.FREAK_create()
        # Dinding image keypoints
        kps = detector.detect(image)
        # Getting first 32 of them. 
        # Number of keypoints is varies depend on image size and color pallet
        # Sorting them based on keypoint response value(bigger is better)
        kps = sorted(kps, key=lambda x: -x.response)[:vector_size]
        # computing descriptors vector
        kps, dsc = descriptor.compute(image, kps)
        # Flatten all of them in one big vector - our feature vector
        dsc = dsc.flatten()
        # Making descriptor of same size
        # Descriptor vector size is 64
        needed_size = (vector_size * 64)
        if dsc.size < needed_size:
            # if we have less the 32 descriptors then just adding zeros at the
            # end of our feature vector
            dsc = np.concatenate([dsc, np.zeros(needed_size - dsc.size)])
    except cv2.error as e:
        print('Error: ', e)
        return None

    return dsc


def batch_extractor(images_path, pickled_db_path="./features.pck"):
    files = [os.path.join(images_path, p) for p in sorted(os.listdir(images_path))]

    result = {}
    for f in files:
        print(f'Extracting features from image {f}')
        name = f.split('/')[-1].lower()
        result[name] = extract_features(f)

    # saving all our feature vectors in pickled file
    with open(pickled_db_path, 'wb') as fp:
        pickle.dump(result, fp)


class Matcher(object):

    def __init__(self, pickled_db_path="./features.pck"):
        with open(pickled_db_path, 'rb') as fp:
            self.data = pickle.load(fp)
        self.names = []
        self.matrix = []
        for k, v in self.data.items():
            self.names.append(k)
            self.matrix.append(v)
        self.matrix = np.array(self.matrix)
        self.names = np.array(self.names)

    def cos_cdist(self, vector):
        # n = 7 + 13 + 2048
        # w = np.ones(n)
        # w[:7] *= 2048 / 7
        # w[7:7+13] *= 2048 / 13
        # w[7+13:7+13+1000] *= 2048 / 1000
        # w[7+13+1000:] *= n / 4 / 2048
        # getting cosine distance between search image and images database
        v = vector.reshape(1, -1)
        # print(v.shape, self.matrix.shape, w.shape)
        return spatial.distance.cdist(self.matrix, v, 'cosine').reshape(-1)

    def match(self, image, topn=5):
        features = extract_features(image=image)
        img_distances = self.cos_cdist(features)
        # getting top 5 records
        nearest_ids = np.argsort(img_distances)[:topn].tolist()
        print(img_distances[nearest_ids])
        nearest_img_paths = self.names[nearest_ids].tolist()

        return nearest_img_paths, img_distances[nearest_ids].tolist()

def show_img(path):
    img = imread(path, mode="RGB")
    plt.imshow(img)
    plt.show()

def run():
    images_path = './dataset/'
    files = [os.path.join(images_path, p) for p in sorted(os.listdir(images_path))]
    # getting 3 random images 
    sample = random.sample(files, 5)

    batch_extractor(images_path)

    # ma = Matcher('./features.pck')

    # for s in sample:
    #    print('Query image ==========================================')
    #    show_img(s)
    #    names, match = ma.match(s, topn=15)
    #    print('Result images ========================================')
    #    for i in range(15):
    #        # we got cosine distance, less cosine distance between vectors
    #        # more they similar, thus we subtruct it from 1 to get match value
    #        print(f'Match {(1-match[i])}')
    #        show_img(os.path.join(images_path, names[i]))

if __name__ == '__main__':
    run()
